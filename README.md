# Project 6: Brevet time calculator with Ajax & MongoDB & REST API
Author: Jonathan Fujii
Contact Address: jfujii@oregon.edu
Description: This implements the RUSA ACP controle time calculator with flask, AJAX, and MongoDB in a RESTful way.
User Instructions: 
Enter information via the provided forms
Any contextual information is provided by existing comments
